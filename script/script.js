const button = document.querySelector('.btn');

button.addEventListener('click', getInfo);

async function getInfo() {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const getIp = await response.json();

        const responce = await fetch(`http://ip-api.com/json/${getIp.ip}`);
        const data = await responce.json();

        const { continent, country, regionName, city, district } = data;

        const values = Object.values({ continent, country, regionName, city, district });
        const keys = ['Континент', 'Країна', 'Регіон', 'Місто', 'Район'];

        const div = document.querySelector('div');
        const list = document.createElement('ul');
        list.classList.add('animated-list');

        values.forEach((value, i) => {
            const listItem = document.createElement('p');
            listItem.innerHTML = `${i + 1}. ${keys[i]}: ${value || 'Undefined'}`;
            list.appendChild(listItem);
        });

        div.appendChild(list);
        button.disabled = true;

        setTimeout(() => {
            list.classList.add('show');
        }, 10);

    } catch (error) {
        console.error('Помилка під час виконання запиту:', error);
    }
}
